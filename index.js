const { is } = require("ramda");
const { dig, clone } = require("svpr");
const uniqid = require("uniqid");

function defaultOpt(options = {}) {
  let opt = Object.assign(
    {
      prefix: "_al",
      uid_key: "key",
      index_key: "index",
      data_key: "data",
      object_marker: "__arrayLike",
      separator: "_",
      itemTransformFn: x => x
    },
    options
  );

  if (!is(Function, opt.itemTransformFn)) {
    opt.itemTransformFn = x => x;
  }
  return opt;
}

// common

function keyCreate(uid, options = {}) {
  let opt = defaultOpt(options);
  if (uid === undefined || uid === null) {
    return randomKey(options);
  } else {
    return `${opt.prefix}${opt.uid_key}${opt.separator}${uid}`;
  }
}

function randomKey(options = {}) {
  let opt = defaultOpt(options);
  return keyCreate(uniqid(), opt);
}

// tests

function isArrayLikeKey(supposed_key, options = {}) {
  let opt = defaultOpt(options);
  return supposed_key.startsWith(keyCreate("", opt));
}

function isArrayLikeObject(obj = {}, options = {}) {
  let opt = defaultOpt(options);
  // return obj.hasOwnProperty(opt.uid_key) && obj[opt.uid_key].startsWith(keyCreate("", opt))
  return obj.hasOwnProperty(opt.data_key);
}

function isArrayLike(obj = {}, options = {}) {
  let opt = defaultOpt(options);
  return obj.hasOwnProperty(opt.object_marker);
}

function containsArrayLike(input = {}, options = {}) {
  let opt = defaultOpt(options);
  return JSON.stringify(input).indexOf(opt.object_marker) !== -1;
}

function containsArrayLikeArray(input = {}, options = {}) {
  let opt = defaultOpt(options);
  let str = JSON.stringify(input);
  return str.indexOf(`"${keyCreate("", opt)}"`) !== -1;
}

// array item

function newItem(data, uid, options) {
  let opt = defaultOpt(options);
  return {
    key: keyCreate(uid, opt),
    data
  };
}

// array

function toObject(arr = [], options = {}) {
  let opt = defaultOpt(options);
  return arr.reduce(
    (obj, item = {}, index) => {
      if (item.hasOwnProperty(opt.data_key)) {
        let key = item[opt.uid_key];
        if (!key || key.length < opt.prefix.length + opt.uid_key.length) {
          key = randomKey(opt);
        }
        Object.assign(obj, {
          [key]: {
            [opt.uid_key]: key,
            [opt.index_key]: +index,
            [opt.data_key]: item.data
          }
        });
      }
      return obj;
    },
    opt.itemTransformFn({
      [opt.object_marker]: true
    })
  );
}

function toObjectDeep(obj = {}, options = {}) {
  let opt = defaultOpt(options);
  return dig(
    (value, index) => {
      // && value.some(v => isArrayLikeObject(v, opt))
      if (value && is(Array, value)) {
        return toObject(value, opt);
      }
      return value;
    },
    { root: obj }
  ).root;
}

const toArrayLike = toObjectDeep;

// object

function toArray(obj = {}, options = {}) {
  let opt = defaultOpt(options);
  return Object.entries(obj)
    .filter(([key, value]) => !key.includes(opt.object_marker))
    .reduce((arr, [key, value]) => {
      if (isArrayLikeKey(key)) {
        arr.push(
          Object.assign(clone(value), {
            [opt.uid_key]: key
          })
        );
      }
      return arr;
    }, [])
    .sort((a, b) => a.index - b.index);
}

function toArrayDeep(obj = {}, options = {}) {
  let opt = defaultOpt(options);
  return dig(
    (value, key) => {
      if (value && is(Object, value) && isArrayLike(value)) {
        return toArray(value, opt);
      }
      return value;
    },
    { root: obj }
  ).root;
}

const fromArrayLike = toArrayDeep;

// convert

function convertToArrayLike(arr = [], options = {}) {
  let opt = defaultOpt(options);
  return Object.entries(arr).reduce(
    (obj, [index, value]) => {
      let key = randomKey(opt);
      Object.assign(obj, {
        [key]: {
          [opt.uid_key]: key,
          [opt.index_key]: +index,
          [opt.data_key]: value
        }
      });
      return obj;
    },
    {
      [opt.object_marker]: true
    }
  );
}

function convert(anyObjOrArr = {}, options = {}) {
  let opt = defaultOpt(options);
  return dig(
    (value, index) => {
      if (value && is(Array, value)) {
        return convertToArrayLike(value, opt);
      }
      return value;
    },
    { root: anyObjOrArr }
  ).root;
}

function revert(obj = {}, options = {}) {
  let opt = defaultOpt(options);
  return dig(
    (value, index) => {
      if (value && is(Object, value) && isArrayLike(value)) {
        return toArray(value, opt).map(item => item.data);
      }
      return value;
    },
    { root: obj }
  ).root;
}

module.exports = {
  convert,
  revert,
  toArrayLike,
  fromArrayLike,
  randomKey,
  keyCreate,
  toObject,
  toObjectDeep,
  toArray,
  toArrayDeep,
  isArrayLike,
  isArrayLikeKey,
  isArrayLikeObject,
  containsArrayLike,
  containsArrayLikeArray,
  newItem
};
